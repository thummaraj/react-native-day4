import React, { Component} from 'react'
import {View, TouchableOpacity,Text} from 'react-native'
import {connect} from 'react-redux'

class Page1 extends Component {
    render() {
        const { todos, completes, addTodo } = this.props
        return (
            <View style={{flex: 1}}>
            <TouchableOpacity onPress={() => {
                addTodo(1)
            }}>
            <Text>Add number to Todo</Text>
            </TouchableOpacity>
            <Text>
                Latest todo is: {todos[todos.length -1]}
            </Text>
            </View>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        todos: state.todos,
        completes: state.completes
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        addTodo: (topic)=>{
            dispatch({
                type: 'ADD_TODO',
                topic: topic
            })
        }
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(Page1)